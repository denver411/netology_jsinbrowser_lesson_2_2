'use strict'

const addProduct = Array.from(document.getElementsByClassName('add'));
let cartQuantityValue = 0;
const cartQuantity = document.getElementById('cart-count');
let cartSumValue = 0;
const cartSum = document.getElementById('cart-total-price');


function cart(event) {
  cartSumValue += Number(event.currentTarget.getAttribute('data-price'));
  cartSum.innerText = getPriceFormatted(cartSumValue);
  cartQuantityValue++;
  cartQuantity.innerText = getPriceFormatted(cartQuantityValue);
}

addProduct.forEach(elem => {
  elem.addEventListener('click', cart);
});