function createContacts() {
  const contactList = JSON.parse(loadContacts());
  let contactsNode = document.getElementsByClassName('contacts-list')[0];
  let nodeTemporary = '';
  // contactsNode.innerHTML = '';
  contactList.forEach(element => {
    nodeTemporary += `<li data-email='${element.email}' data-phone='${element.phone}'><strong>${element.name}</strong></li>`;
  });
  contactsNode.innerHTML = nodeTemporary;
}


document.addEventListener('DOMContentLoaded', createContacts);